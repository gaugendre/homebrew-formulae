class AercContacts < Formula
  desc "Search macOS Contacts and output data for aerc email client"
  homepage "https://git.augendre.info/gaugendre/aerc-contacts-macos"
  url "https://git.augendre.info/gaugendre/aerc-contacts-macos.git", :tag => "0.1.0", :using => :git, :revision => "58579a5ed7b3afdbddd84ea769701a5c4802eb4c"
  sha256 "d415b1ac07865941fe3db77d2f1d2888d47f03759959bde47bbc7b6f2146945c"
  license "MIT"
  head "https://git.augendre.info/gaugendre/aerc-contacts-macos.git", :using => :git

  depends_on :xcode => ["10.0", :build]

  def install
    bin.mkpath
    system "make", "install", "prefix=#{prefix}"
  end

  test do
    system "#{bin}/aerc-contacts JohnDoe"
  end
end

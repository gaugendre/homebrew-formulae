# crocmagnon/homebrew-formulae

## Usage
```shell script
brew tap crocmagnon/formulae https://git.augendre.info/gaugendre/homebrew-formulae.git
brew install <formula_name>

# For example:
brew install aerc-contacts
```


# Reuse
If you do reuse my work, please consider linking back to this repository 🙂